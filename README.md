
Instagram Stories Sharing for Cordova
======

This is a simple plugin that allows to share content to Instagram Stories using Facebook's API Documentation: https://developers.facebook.com/docs/instagram/sharing-to-stories/

## Installation

`cordova plugin add https://bitbucket.org/claytonlow/cordova-plugin-instagram-stories`

## Usage

```
IGStory.shareToStory(
    opts,
    success => {
      console.log(success);
    },
    err => {
      console.error(err);
    }
  );
```

### Options

| optionKey  |  Type  |  Description  |
|---|---|---|
| backgroundImage  | string (Optional) | Local Url to be the  background Image  |
|  stickerImage | string (Optional)   | Local Url to be the  sticker Image  |
|  attributionURL |  string (Optional) |  A link back to the app when a user clicks it (this is a beta feature and requires approval from Facebook |
|  backgroundTopColor |  string (Optional) |  A hex color to be used when you don't pass a backgroundImage. If you pass both, backgroundImage is used and this is disregarded |
|  backgroundBottomColor |  string (Optional) |  A hex color to be used when you don't pass a backgroundImage. If you pass both, backgroundImage is used and this is disregarded |
|  backgroundVideo |  string (Optional) |  Local Url to be the backgroundVideo |
